import java.util.Scanner;
public class FunHouseOfMirrors {
	
	public static void main(String[] args) {
		Scanner inNum = new Scanner(System.in);
		Scanner inStr = new Scanner(System.in);
		System.out.print("4 < L < 21\nL = ");
		int l = inNum.nextInt();
		System.out.print("4 < W < 21\nW = ");
		int w = inNum.nextInt();
		char[][] room = new char[w][l];
		boolean door = false, noExit = true;
		String temp = "";
		if(4 < w && w < 21 && 4 < l && l < 21) {
			int x = 0, y = 0;

			/*  
			 * Define the door wall position as the starting point and the going direction
			 * so i can figure out what way too look for the mirrors or set the exit door
			 * 0 -> left to right
			 * 1 -> top to down
			 * 2 -> right to left
			 * 3 -> down to top
			 */
			byte direction = 0;
			char theChar = '&';
			System.out.println("Now you'll build the house with:\nx - representing the wall"+
					"\n. - reprezenting the empty space\n* - representing the enterance"+
					"/ and \\ - representing the mirrors and \n& - will be the exit door.");
			for(int i = 0; i < w; i++) {
				System.out.format("Row %d : ", i+1);
				if((temp = inStr.nextLine().toLowerCase()).length() < l+1) {
					for(int j = 0; j < l; j++) {
						theChar = temp.charAt(j);
						if(theChar == '.' || theChar == '/' || theChar == '\\' ||
								(( i == 0 || i == w-1 || j == 0 || j == l-1) && (theChar == 'x' || theChar == '*'))) {
							room[i][j] = theChar;
							if(theChar == '*' && !door) {
								door = true;
								x = i;
								y = j;
								if(j == 0) {
									direction = 0;
								} else if(i == 0) {
									direction = 1;
								} else if(j == l-1) {
									direction = 2;
								} else {
									direction = 3;
								}
							}
						} else {
							System.out.println("Sorry, start over :P!");
							return;
						}
					}
				} else {
					System.out.println("Try again the last row of numbers!");
					i--;
				}
			}

			if(door) {
				String doorWall = "";
				if(direction == 0) {
					theChar = room[x][++y];
					doorWall = "Left";
				} else if(direction == 1) {
					theChar = room[++x][y];
					doorWall = "Top";
				} else if(direction == 2) {
					theChar = room[x][--y];
					doorWall = "Right";
				} else if(direction == 3) {
					theChar = room[--x][y];
					doorWall = "Bottom";
				}
				
				while(noExit) {
					if(direction == 0) {
						if(theChar == '/') {
							direction = 3;
							theChar = room[--x][y];
						} else if(theChar == '\\') {
							direction = 1;
							theChar = room[++x][y];
						} else {
							theChar = room[x][++y];
						}
					} else if(direction == 1) {
						if(theChar == '/') {
							direction = 2;
							theChar = room[x][--y];
						} else if(theChar == '\\') {
							direction = 0;
							theChar = room[x][++y];
						} else {
							theChar = room[++x][y];
						}
					} else if(direction == 2) {
						if(theChar == '/') {
							direction = 1;
							theChar = room[++x][y];
						} else if(theChar == '\\') {
							direction = 3;
							theChar = room[--x][y];
						} else {
							theChar = room[x][--y];
						}
					} else if(direction == 3) {
						if(theChar == '/') {
							direction = 0;
							theChar = room[x][++y];
						} else if(theChar == '\\') {
							direction = 2;
							theChar = room[x][y--];
						} else {
							theChar = room[--x][y];
						}
					}
					if(theChar == 'x' && 
							(!(y == 0 && doorWall == "Left") ||
							!(x == 0 && doorWall == "Top") ||
							!(y == l-1 && doorWall == "Right") ||
							!(x == w-1 && doorWall == "Bottom"))
							) {
						room[x][y] = '&';
						noExit = false;
					}
				}
			}
		} else {
			System.out.println("Read the instructions!\nStart the app again or maybe i'll keep you in a loop next time!");
		}
		for(int i = 0; i < w; i++) {
			for(int j = 0; j < l; j++) {
				System.out.print(room[i][j]);
			}
			System.out.println();
		}
		
	}
	
}
