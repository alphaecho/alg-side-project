import java.util.Scanner;

public class PascalsTravels {
	static int paths = 0;
	
	public static void main(String[] args) {
		Scanner inNum = new Scanner(System.in);
		Scanner inStr = new Scanner(System.in);
		System.out.print("Type a number 3 < n < 21\nn = ");
		int n = inNum.nextInt();
		int[][] matrix = new int[n][n];
		String temp = "";
		if(3 < n && n < 21) {
			System.out.format("Type %d numbers in a row. (no spaces)\n", n);
			for(int i = 0; i < n; i++) {
				System.out.format("Row %d : ", i+1);
				if((temp = inStr.nextLine()).length() < n+1 && isInt(temp)) {
					for(int j = 0; j < n; j++) {
						matrix[i][j] = Integer.parseInt(String.valueOf(temp.charAt(j)) );
					}
				} else {
					System.out.println("Try again the last row of numbers!");
					i--;
				}
			}
			paths(0, 0, n, matrix);
			System.out.format("Total paths %d ", paths);
		} else {
			System.out.println("You've made a mistake, now you have to start over :P");
		}
	}
	
	public static void paths(int i, int j, int n, int[][] M) {
		if(j+M[i][j] < n && !(i == n-1 && j == n-1)) {
			paths(i, j+M[i][j], n, M);
		}
		if(i+M[i][j] < n && !(i == n-1 && j == n-1)) {
			paths(i+M[i][j], j, n, M); 
		}
		paths += (i == n-1 && j == n-1) ? 1 : 0;
	}
	
	public static boolean isInt(String str) { 
		try {
			Integer.parseInt(str);  
			return true;
		} catch(NumberFormatException e){  
			return false;  
		}  
	}
}